# 概述

Elasticsearch的增删改查操作全部由http接口完成。由于Elasticsearch授权模块需要付费，所以免费开源的Elasticsearch可能存在未授权访问漏洞。

该漏洞导致攻击者可以拥有Elasticsearch的所有权限。可以对数据进行任意操作。业务系统将面临敏感数据泄露、数据丢失、数据遭到破坏甚至遭到攻击者的勒索。

Elasticsearch服务普遍存在一个未授权访问的问题，攻击者通常可以请求一个开放9200或9300的服务器进行恶意攻击。




处理措施：

1、 es业务端口无必要不对外开放

2、 es业务开启认证





## elastcisearch内置用户

[内置用户](https://www.elastic.co/guide/en/elasticsearch/reference/current/built-in-users.html)


> 内置用户用于特定目的，不用于一般业务用途。**尤其注意不要使用 elastic 这个超级账号来进行实际业务，它拥有对集群的完全访问权限**。
        
> 正确的用法是使用 elastic 用户来创建普通业务账号，来进行业务索引数据的crud



elasticsearch共有如下内置账号：


- elastic  内置的超级账号

        任何可以以elastic用户身份登录的人都可以直接只读访问受限索引，例如.security. 此用户还可以管理安全性并创建具有无限权限的角色。

- kibana_system  Kibana 用于与 Elasticsearch 连接和通信的用户。

- logstash_system  在 Elasticsearch 中存储监控信息时使用的用户 Logstash。
- beats_system      Beats 在 Elasticsearch 中存储监控信息时使用的用户。
- apm_system          APM 服务器在 Elasticsearch 中存储监控信息时使用的用户。
- remote_monitoring_user  Metricbeat 在 Elasticsearch 中收集和存储监控信息时使用的用户。它具有remote_monitoring_agent和 remote_monitoring_collector内置角色。




