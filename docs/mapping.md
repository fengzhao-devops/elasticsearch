# 概述

Mapping是Elasticsearch中一个非常重要的概念，它定义了搜索引擎应该如何存储和搜索文档及其字段。

mapping是定义了document如何组织数据的过程。

每个文档（document）有一系列值（field），每个field都有其数据类型，当mapping数据时，
