# 身份认证和鉴权

ES作为一款当下非常流行的轻量级存储搜索引擎，其安全性也变得日益重要。否则就会非常容易造成敏感数据泄露的严重问题。主要是因为使用者并没有将ES的安全功能打开。


1. ES在默认安装后，不提供任何形式的安全防护（ES8.0之后，默认是开启身份验证的）;

2. 错误的配置信息导致公网可以访问ES集群;

   ----  在elasticsearch.yml 的配置文件中，server.host被错误的配置为0.0.0.0



Elasticsearch 支持哪种类型的身份验证？如何设置？如何确保用户不会看到他们不应看到的数据？

简单来说，如果用户或 API 想访问 Elastic，其必须完成身份验证。

Elasticsearch 为多种安全方法提供原生支持，例如：

- 原生用户身份验证
- 活动目录用户身份验证
- 基于文件的用户身份验证
- LDAP 用户身份验证
- PKI 用户身份验证
- SAML 身份验证
- Kerberos 身份验证



ElasticSearch于6.8及7.1版本开始提供免费的x-pack, 并已默认集成，只需通过简单的配置即可开启。

我们知道 Elastic 安全是非常重要的。没有这个我们的数据可以被任何的人进行访问，修改，删除。Elastic Stack 的安全是由 x-pack 所提供的。

在 Elastic Stack 7.0 版本之前，这个是商用的版本，需要进行安装，并购买。从Elastic Stack 7.0之后，x-pack 都已经在发布版中，所以不需要进行安装。我们只需要进行配置就可以了。

通常我们的ES节点部署在内网当中，9200等业务端口只对业务服务器开放，不需要对外部开放。kibana是一款非常强大的可视化工具，devTools使开发人员可以方便的操作集群，索引，但是这个页面非开发人员也是可以看到的，因此第一步就是先要屏蔽非es使用方，提供一个登录认证功能。


### 设置配置文件

在集群中的每个节点都添加如下配置：

```shell
# 允许认证
xpack.security.enabled: true
# 认证方式
xpack.license.self_generated.type: basic
xpack.security.transport.ssl.enabled: true
xpack.security.transport.ssl.verification_mode: certificate
xpack.security.transport.ssl.keystore.path: elastic-certificates.p12
xpack.security.transport.ssl.truststore.path: elastic-certificates.p12
```


生成 TLS 和身份验证，将会在 config 下生成 elastic-certificates.p12 文件，将此文件传到其他两个节点的 config 目录，注意文件权限：

```shell
[estestuser@vm-10-201-42-9 elasticsearch-7.1.1]$ bin/elasticsearch-certutil cert -out config/elastic-certificates.p12 -pass ""
WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by org.bouncycastle.jcajce.provider.drbg.DRBG (file:/home/estestuser/elasticsearch-7.1.1/lib/tools/security-cli/bcprov-jdk15on-1.61.jar) to constructor sun.security.provider.Sun()
WARNING: Please consider reporting this to the maintainers of org.bouncycastle.jcajce.provider.drbg.DRBG
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release
This tool assists you in the generation of X.509 certificates and certificate
signing requests for use with SSL/TLS in the Elastic stack.

The 'cert' mode generates X.509 certificate and private keys.
    * By default, this generates a single certificate and key for use
       on a single instance.
    * The '-multiple' option will prompt you to enter details for multiple
       instances and will generate a certificate and key for each one
    * The '-in' option allows for the certificate generation to be automated by describing
       the details of each instance in a YAML file

    * An instance is any piece of the Elastic Stack that requires a SSL certificate.
      Depending on your configuration, Elasticsearch, Logstash, Kibana, and Beats
      may all require a certificate and private key.
    * The minimum required value for each instance is a name. This can simply be the
      hostname, which will be used as the Common Name of the certificate. A full
      distinguished name may also be used.
    * A filename value may be required for each instance. This is necessary when the
      name would result in an invalid file or directory name. The name provided here
      is used as the directory name (within the zip) and the prefix for the key and
      certificate files. The filename is required if you are prompted and the name
      is not displayed in the prompt.
    * IP addresses and DNS names are optional. Multiple values can be specified as a
      comma separated string. If no IP addresses or DNS names are provided, you may
      disable hostname verification in your SSL configuration.

    * All certificates generated by this tool will be signed by a certificate authority (CA).
    * The tool can automatically generate a new CA for you, or you can provide your own with the
         -ca or -ca-cert command line options.

By default the 'cert' mode produces a single PKCS#12 output file which holds:
    * The instance certificate
    * The private key for the instance certificate
    * The CA certificate

If you specify any of the following options:
    * -pem (PEM formatted output)
    * -keep-ca-key (retain generated CA key)
    * -multiple (generate multiple certificates)
    * -in (generate certificates from an input file)
then the output will be be a zip file containing individual certificate/key files


Certificates written to /home/estestuser/elasticsearch-7.1.1/config/elastic-certificates.p12

This file should be properly secured as it contains the private key for 
your instance.

This file is a self contained file and can be copied and used 'as is'
For each Elastic product that you wish to configure, you should copy
this '.p12' file to the relevant configuration directory
and then follow the SSL configuration instructions in the product guide.

```










